<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	var $defaultHome;
	public function __construct() {
		parent::__construct();
		$this->load->model('loginModel');
        // Load form helper library
    $this->load->helper('form');

    // Load form validation library
    $this->load->library('form_validation');

    // Load session library
		$this->load->library('session');

	 $this->defaultHome = 'Home';
	}



  public function index() {
		$this->load->view('login');
  }

	// Check for user login process
  public function user_login_process() {

    $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');


      if(isset($this->session->userdata['logged_in'])) {
				$data = $this->session->userdata['logged_in'];
				echo $this->defaultHome;
        redirect($this->defaultHome);
      }else {
        $data = array(
          'username' => $this->input->post('username'),
          'password' => $this->input->post('password')
          );
        $result = $this->loginModel->login($data);
        if ($result == TRUE) {

          $username = $this->input->post('username');
          $result = $this->loginModel->read_user_information($username);

          if ($result != false) {
            $session_data = array(
            'UserCode' => $result[0]->UserCode,
            'Image' => $result[0]->Image,
            'Role' => $result[0]->Role,
            'Id' => $result[0]->Id,
            );
            // Add user data in session
            $this->session->set_userdata('logged_in', $session_data);
            redirect($defaultHome);

          }
        } else {
          $data['message_display'] = 'Invalid Username or Password';

          $this->load->view('login', $data);
        }
      }

  }

    // Logout from admin page
  public function logout() {

    // Removing session data
    $sess_array = array(
    'username' => ''
    );
    $this->session->unset_userdata('logged_in', $sess_array);
    $data['message_display'] = 'Successfully Logout';
    $this->load->view('login', $data);
  }
}
