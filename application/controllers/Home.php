<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	var $data;
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('HomeModel','hv');
		$this->data['controller'] = 'Home';
		$this->data['home'] = 'Driving License Test';
	}
/**
 * dashboard functionallity
 */
	public function index()
	{
		$data = [
			'active' => 'Home_dash',
			'lmv_count' => $this->hv->get_reg_count('LMV'),
			'two_wheel' => $this->hv->get_reg_count('Motor Cycle'),
			'three_wheel' => $this->hv->get_reg_count('Three Wheeler'),
			'last_five_data' => $this->hv->get_last_readings('5'),
			'additional_js'=> ['raphael.min.js','morris.min.js'],
			'additional_css' => ['morris.css']
		];

		$data = array_merge($data,$this->data);
		$this->load->view('Home/home',$data);
	}

	/**
	 * capture list
	 */
	public function lmv($type) {
		$register_list = $this->hv->registration_list('',$type);
		$data = [
			'title'	=> 'LMV',
			'action' => 'View',
			'active' => $type,
			'list_data' => $register_list,
			'additional_css' => ['custom_style.css']
			];
			$data = array_merge($data,$this->data);
		$this->load->view('Home/lmv',$data);
	}


	/**
	 * add screen for Registration
	 */
	public function registration($edit = null) {

		$data = [
			'active' => 'registration',
			'title'	=> 'Registration',
			'action' => 'Add',
		];
		if($edit != null) {
			$data['fetch_data'] = $this->hv->registration_list($edit);
		}
		$data = array_merge($data,$this->data);
		$this->load->view('Home/registration',$data);
	}

	/**
	 * save Registration
	 * @param name,learners_no,test_type,dob,age
	 */
	public function save_registration() {
		$post_data = $this->input->post();
		$return  = $this->hv->save_registration($post_data);
		if($return==true) {
			redirect(base_url('/Home/registration_list'));
		}else{
			$this->load->view('Home/registration',$data);
		}
	}

	/**
	 * list screen for camera
	 */
	public function registration_list() {
		$list_data = $this->hv->registration_list();
		$data = [
			'active' => 'registration_list',
			'title'	=> 'Registration',
			'action' => 'List',
			'list_data' => $list_data
		];
		$data = array_merge($data,$this->data);
		$this->load->view('/Home/registration_list',$data);
	}

	/**
	 * delete camera
	 */
	public function delete_registration($id) {
		$this->hv->delete_registration($id);
		redirect(base_url('/Home/registration_list'));
	}

	/***************************************report module start**********************************/
	public function select_report() {
		$data = [
			'active' => 'report',
			'title'	=> 'Driving ',
			'action' => 'Report',
		];
		$data = array_merge($data,$this->data);
		$this->load->view('Home/report',$data);
	}
	public function report() {
		$post = $this->input->post();
		$get_data = $this->hv->report($post);
		$data = [
			'active' => 'report',
			'title'	=> 'Driving ',
			'action' => 'Report',
			'report_data' => $get_data
		];
		$data = array_merge($data,$this->data);
		$this->load->view('Home/report',$data);
	}
}
