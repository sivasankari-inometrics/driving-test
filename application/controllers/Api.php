<?php
/**
 * NOTE: Entry point of API
 * API for getting data from devices.
 */
class Api extends CI_Controller {

  public function __construct() {
		parent::__construct();
    $this->load->library('session');
    $this->load->model('api_model', 'api');
  }

  /**
   * login authentication
   * @return token,code,message
   */
  public function get_device_data() {
    $output = [
      'code' => 500,
      'success' => false,
      'content' => [
        'message' => 'not processed'
      ]
    ];
    try {
			parse_str($_SERVER['QUERY_STRING'], $_GET);
			$data = $this->api->get_device_data($_GET);
			$this->sendPush('HomeLive-data', $_GET);
      if ($_GET != null) {

        $output['success'] = true;
        $output['code'] = 200;
        $output['content']['data'] = 'Data Received';
        $output['content']['message'] = 'Success';

      } else {
        throw new Exception('null', 400);
      }
    } catch (Exception $error) {
      $output['success'] = false;
      $output['code'] = $error->getCode();
      $output['content']['message'] = $error->getMessage();
    }
    $output['content']['code'] = $output['code'];
    return $this->output
                ->set_content_type('application/json')
                ->set_status_header($output['code'])
                ->set_output(json_encode($output['content']));
	}

	/**
	 * set device_data
	 */
	public function set_device_data() {
		$output = [
      'code' => 500,
      'success' => false,
      'content' => [
        'message' => 'not processed'
      ]
    ];
    try {
			parse_str($_SERVER['QUERY_STRING'], $_GET);
			$data = $this->api->set_device_data($_GET);
      if ($data != null) {

        $output['success'] = true;
        $output['code'] = 200;
				$output['content']['data'] = 'Data Received';
				$output['content']['time_intreval'] = $data['time_intreval'];
				$output['content']['threshold_value'] = $data['threshold_value'];
        $output['content']['message'] = 'Success';

      } else {
        throw new Exception('null', 400);
      }
    } catch (Exception $error) {
      $output['success'] = false;
      $output['code'] = $error->getCode();
      $output['content']['message'] = $error->getMessage();
    }
    $output['content']['code'] = $output['code'];
    return $this->output
                ->set_content_type('application/json')
                ->set_status_header($output['code'])
                ->set_output(json_encode($output['content']));

	}

	/**
	 * set live push connection
	 * get channel written in js
	 * return push data to dashoard
	 */
	private function sendPush($channel, $data) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://rest.ably.io/channels/'.$channel.'/publish');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, 'data='.json_encode($data));
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_USERPWD, 'b8vWRQ.CzEN7Q:KOIRTvIzvbbKgmX1');
    $headers = array();
    $headers[] = 'Content-Type: application/x-www-form-urlencoded';
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($curl);
    // if (curl_errno($curl)) {
    //   throw new Exception(curl_error($curl), 401);
    // }
    curl_close ($curl);
    return;
  }
}
?>
