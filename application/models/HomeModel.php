<?php
class HomeModel extends CI_Model{
  function __construct() {

		parent::__construct();
		$this->load->database();
		$this->device = $this->db->get_where('settings',array('key_field'=>'default_device'))->row()->key_value;
		$this->today = date('Y-m-d');
	}



	//************************************ dashboard module ******************************************//
	// public function get_default_device() {

	// 	return $this->db->get_where('lastreading',array('device_id'=>$this->device))->row();
	// }

	// public function get_today_data() {

	// 	$result = $this->db->query("SELECT count(*) as count,max(temperature) as max_temp FROM device_readings WHERE date(created_time)='$this->today'")->result();
	// 	$data['capture_count'] = $result[0]->count;
	// 	$data['max_temp'] = $result[0]->max_temp;
	// 	return $data;
	// }
	// public function get_total_device() {
	// 	$result = $this->db->query("SELECT count(DISTINCT name) as camera_count,count(DISTINCT location) as location_count FROM device")->result();
	// 	return $result[0];
	// }

	// public function get_actual_data() {
	// 	return $this->db->get_where('device_readings',array('device_id'=>$this->device,'date(created_time)'=>'2019-05-15'))->result();
	// }
	// public function get_threshold_limit() {
	// 	return $this->db->get_where('settings',array('key_field'=>'threshold_count'))->row()->key_value;

	// }
	// public function get_threshold_data($limit) {
	// 	$th_limit = $this->get_threshold_limit();
	// 	return $this->db->query("select * from device_readings where device_id='$this->device' and date(created_time)='2019-05-15' and people_count>= $th_limit order by id desc limit $limit ")->result();
	// }
	// /**
	//  * capture list
	//  */
	// public function capture_list($limit=null) {
	// 	if($limit==null) {
	// 		$limit = '10';
	// 	}
	// 	return $this->db->order_by('id', 'desc')->get_where('device_readings',array('device_id'=>$this->device),$limit)->result();
	// }
	//*********************************** dashboard module ***************************************//

	//********************************* registration module **************************************//

	/**
	 * save registration details
	 * table - driving_registration
	 */
	public function save_registration($post_data) {
		if($post_data["id"]!=null) {
			$this->db->where('id',$post_data["id"]);
			$query = $this->db->update('driving_registration', $post_data);
		}else {
			$query = $this->db->insert('driving_registration', $post_data);
		}
    return $query ? true : false;
	}

	/**
	 * select registration_list
	 */
	public function registration_list($id=null,$type=null) {
		if($id!=null) {
			$data = $this->db->get_where('driving_registration',array('id'=>$id))->row();
		}else {
			if($type!=null) {
				$array = array('test_type'=>$type,'status'=>'1');
			}else {
				$array = array();
			}
			$data = $this->db->get_where('driving_registration',$array)->result();
		}
		if($data!=null) {
				return $data;
		}
	}

	/**
	 * delete camera
	 */
	public function delete_registration($id) {

		$this->db->where('id',$id);
		$this->db->delete('driving_registration');
	}
	/**
	 * get total registration count test wise
	 */
	public function get_reg_count($type=null) {
		$date = date('Y-m-d');
		$where = "";
		if($type!=null) {
			$where = " and test_type like '$type%'";
		}
		$result = $this->db->query("SELECT count(id) as test_count FROM driving_registration where date(created_at)='$date' $where")->result();
		return $result[0]->test_count;
	}
	public function get_last_readings($limit) {
		$date = date('Y-m-d');
		return $this->db->order_by('id', 'desc')->get_where('driving_registration',array(),$limit)->result();
	}
	//************************************** report module ******************************************//

	public function	report($data) {
		$todate = date('Y-m-d',strtotime($data['to_date']));
		$fromdate = date('Y-m-d',strtotime($data['from_date']));
		$data = $this->db->query("select * from driving_registration where date(created_at) between '$fromdate' and '$todate'")->result();
				return $data;

	}
}
