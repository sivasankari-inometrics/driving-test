<?php
class Api_model extends CI_Model{
  function __construct() {

		parent::__construct();
		$this->load->database();
	}
	public function settings($keyfield) {
		return $this->default_device = $this->db->get_where('settings',array('key_field'=>$keyfield))->row()->key_value;
	}
  public function get_device_data($data) {

		$today = date('Y-m-d');
		$device_id = $data['device_id'];
		$this->db->where('device_id',$device_id);
		$this->db->update('lastreading',$data);
		$this->db->insert('device_readings',$data);
	}

	public function set_device_data($data) {

		if($data['flag']=='1') {
			$this->db->where('key_field','default_device');
			$this->db->update('settings',$data['device_id']);
		}
		$rtn['time_intreval'] = $this->settings('default_interval');
		$rtn['threshold_value'] = $this->settings('threshold_count');
		return $rtn;
	}
}
