<!DOCTYPE html>
<html lang="en">
<?php
if (!isset($this->session->userdata['logged_in'])) {

	redirect(base_url()."Login");

}
?>
<head>
    <title>Driving Test</title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content=""/>
    <meta name="author" content=""/>

    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url('/assets/images/favicon.ico')?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?= base_url('/assets/fonts/fontawesome/css/fontawesome-all.min.css')?>">
    <!-- animation css -->
    <link rel="stylesheet" href="<?= base_url('/assets/plugins/animation/css/animate.min.css')?>">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?= base_url('/assets/css/style.css')?>">
	<?php
	if(isset($additional_css)) {
		foreach($additional_css as $css){ ?>
<link rel="stylesheet" href="<?=base_url('/assets/css/'.$css)?>">
<?php		}
	}
	?>
</head>
