<body>
<style>
.side_bar_style {
	top: 10px;
    padding-right: 10px;
    margin: 9px 5px;
    border-radius: 10px;
    background: #352b2b;
    color: #FFF;
}
</style>
<a href="<?= base_url('login/logout'); ?>" class="dud-logout" title="Logout" style="float:right">
                                    <i class="feather icon-log-out">Logout</i>
                                </a>
    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- [ Pre-loader ] End -->
    <!-- [ navigation menu ] start -->
    <nav class="pcoded-navbar">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a href="index.html" class="b-brand">
                    <div class="b-bg">
                        <i class="feather icon-users"></i>
                    </div>
                    <span class="b-title">Driving Test</span>
                </a>
                <a class="mobile-menu" id="mobile-collapse" href="javascript:"><span></span></a>
            </div>


            <div class="navbar-content scroll-div">
                <ul class="nav pcoded-inner-navbar">



                    <li class="nav-item pcoded-menu-caption">
                        <label>Home</label>
                    </li>
										<li class="nav-item <?=  $active=='Home_dash'?'active':'' ?>">
                        <a href="<?= base_url('Home');?>" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-home"></i></span>
												<span class="pcoded-mtext">Dashboard</span>
											</a>
										</li>
                    <li class="nav-item pcoded-hasmenu <?= ($active=='Home_cam_add'|| $active=='Home_cam')?'pcoded-trigger':'' ?>">
											<a href="javascript:" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-file-text"></i></span>
												<span class="pcoded-mtext">Registration</span>
											</a>
											<ul class="pcoded-submenu" style ="<?= ($active=='registration'||$active=='registration_list')?'display:block':'' ?>">
												<li class="<?= $active=='registration'?'active':'' ?>"><a href="<?= base_url('/Home/registration');?>" class="">Registration</a></li>
												<li class="<?= $active=='registration_list'?'active':'' ?>"><a href="<?= base_url('/Home/registration_list');?>" class="">Registration List</a></li>
											</ul>
                    </li>
					<li class="nav-item pcoded-menu-caption">
                        <label>Test</label>
                    </li>
					<li class="nav-item <?=  $active=='LMV'?'active':'' ?>">
                        <a href="<?= base_url($controller.'/lmv/LMV')?>" class="nav-link ">
												<span class="pcoded-micon"><i class="fa fa-car"></i></span>
												<span class="pcoded-mtext">LMV</span>
											</a>
										</li>
										<li class="nav-item <?=  $active=='Motor Cycle(Without Gear)'?'active':'' ?>">
                        <a href="<?= base_url('Home');?>" class="nav-link ">
												<span class="pcoded-micon"><i class="fa fa-motorcycle"></i></span>
												<span class="pcoded-mtext">Motor Cycle(Without Gear)</span>
											</a>
										</li>
										<li class="nav-item <?=  $active=='Motor Cycle(With Gear)'?'active':'' ?>">
                        <a href="<?= base_url('Home');?>" class="nav-link ">
												<span class="pcoded-micon"><i class="fa fa-bicycle"></i></span>
												<span class="pcoded-mtext">Motor Cycle(With Gear)</span>
											</a>
										</li>
										<li class="nav-item <?=  $active=='Three Wheeler'?'active':'' ?>">
                        <a href="<?= base_url('Home');?>" class="nav-link ">
												<span class="pcoded-micon"><i class="fa fa-taxi"></i></span>
												<span class="pcoded-mtext">Three Wheeler</span>
											</a>
										</li>
					<li class="nav-item pcoded-hasmenu <?= ($active=='people_count_report')?'pcoded-trigger':'' ?>">
											<a href="javascript:" class="nav-link ">
												<span class="pcoded-micon"><i class="feather icon-box"></i></span>
												<span class="pcoded-mtext">Report</span>
											</a>
											<ul class="pcoded-submenu" style="<?= ($active=='people_count_report')?'display:block':'' ?>">
												<li class="<?= $active=='people_count_report'?'active':'' ?>"><a href="<?= base_url('/Home/select_people_count');?>" class="">People Count Report</a></li>
											</ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->


    <!-- [ Header ] end -->
