<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
					<!-- [ breadcrumb ] start -->
				<div class="page-header">
					<div class="page-block">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="page-header-title">
										<h5 class="m-b-10"><?= $title ?></h5>
								</div>
								<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="javascript:"><?= $home.' / '.$title ?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
					<!-- [ breadcrumb ] end -->
				<div class="main-body">
					<div class="page-wrapper">
							<!-- [ Main Content ] start -->
						<div class="row">
							<div class="col-sm-12">
								<div class="card">
									<div class="card-header">
											<h5><?= $title.' - '.$action ?></h5>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-6">
												<form action="<?=base_url($controller.'/save_registration') ?>" method="post">
													<div class="form-group">
															<label for="exampleInputEmail1">Name</label>
															<input type="text" class="form-control"   placeholder="Name" name="name" value="<?= isset($fetch_data->name)?$fetch_data->name:'' ?>">
													</div>

													<div class="form-group">
														<label >Learners License Number</label>
															<input type="text" class="form-control"   placeholder="Learners License Number" name="l_license_no" value="<?= isset($fetch_data->l_license_no)?$fetch_data->l_license_no:'' ?>" required>
													</div>
													<div class="form-group">
														<label >Date Of Birth</label>
															<input type="date" class="form-control"   placeholder="Date Of Birth" name="dob" value="<?= isset($fetch_data->dob)?$fetch_data->dob:'' ?>">
													</div>
													<!-- <div class="form-group">
														<label >Age</label>
															<input type="text" class="form-control"   placeholder="Age" name="age" value="</?= isset($fetch_data->age)?$fetch_data->age:'' ?>">
													</div> -->
													<div class="form-group">
														<label >Test Type</label>
														<select class="form-control" name="test_type" required>
														<option>-----------------------------------------select-------------------------------------------</option>

															<option <?= isset($fetch_data->test_type)? $fetch_data->test_type=='LMV' ? 'selected' : '' : '' ?>>LMV</option>
															<option <?= isset($fetch_data->test_type)? $fetch_data->test_type=='Motor Cycle(without Gear)' ? 'selected' : '' : '' ?>>Motor Cycle(without Gear)</option>
															<option <?= isset($fetch_data->test_type)? $fetch_data->test_type=='Motor Cycle(with Gear)' ? 'selected' : '' : '' ?>>Motor Cycle(with Gear)</option>
															<option <?= isset($fetch_data->test_type)? $fetch_data->test_type=='Three Wheeler' ? 'selected' : '' : '' ?>>Three Wheeler</option>

														</select>
													</div>
													<input type="hidden" name="id" value="<?=isset($fetch_data->id)?$fetch_data->id:'' ?>">
													<button  class="btn btn-primary" id="button">Submit</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- [ Main Content ] end -->
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
	<!-- [ Main Content ] end -->
	<?= include('application/views/include/footer.php'); ?>
