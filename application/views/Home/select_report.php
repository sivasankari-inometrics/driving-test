<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
					<!-- [ breadcrumb ] start -->
				<div class="page-header">
					<div class="page-block">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="page-header-title">
										<h5 class="m-b-10"><?= $title ?></h5>
								</div>
								<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="javascript:"><?= $home.' / '.$title ?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
					<!-- [ breadcrumb ] end -->
				<div class="main-body">
					<div class="page-wrapper">
							<!-- [ Main Content ] start -->
						<div class="row">
							<div class="col-sm-12">
								<div class="card">
									<div class="card-header">
											<h5><?= $title.' - '.$action ?></h5>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-6">
												<form action="<?=base_url($controller.'/people_count_report') ?>" method="post">
													<div class="form-group">
															<label for="exampleInputEmail1">From Date</label>
															<input type="date" class="form-control"   placeholder="From Date" name="from_date" value="" required autocomplete="off">
													</div>
													<div class="form-group">
															<label for="exampleInputEmail1">To Date</label>
															<input type="date" class="form-control"   placeholder="To Date" name="to_date" value="" required autocomplete="off">
													</div>
													<button  class="btn btn-primary" id="button">Submit</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- [ Main Content ] end -->
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
	<!-- [ Main Content ] end -->
	<?= include('application/views/include/footer.php'); ?>
