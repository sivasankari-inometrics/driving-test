<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>
<style>
.device_count {
	padding: 16px 25px;
}
</style>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->

                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <!--[ daily sales section ] start-->
                                <div class="col-md-6 col-xl-4">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">Two Wheeler</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-9">
                                                    <h3 class="f-w-300 d-flex align-items-center m-b-0 "><i class="fa fa-motorcycle text-c-green f-30 m-r-10"></i><p class="f-w-300 m-b-0 people_count" style="font-size: 26px;"><?= $two_wheel ?></p></h3>
                                                </div>

                                                <div class="col-3 text-right">
                                                    <p class="m-b-0"></p>
                                                </div>
                                            </div>
                                            <div class="progress m-t-30" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--[ daily sales section ] end-->
                                <!--[ Monthly  sales section ] starts-->
                                <div class="col-md-6 col-xl-4">
                                    <div class="card Monthly-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">Three Wheeler</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-9">
                                                    <h3 class="f-w-300 d-flex align-items-center  m-b-0 "><i class="fa fa-truck text-c-red f-30 m-r-10"></i><p class="f-w-300 m-b-0 temperature" style="font-size: 26px;"><?= $three_wheel ?></p>°C</h3>
                                                </div>
                                                <div class="col-3 text-right">
                                                    <p class="m-b-0"></p>
                                                </div>
                                            </div>
                                            <div class="progress m-t-30" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme2" role="progressbar" style="width: 100%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="col-md-6 col-xl-4">
                                    <div class="card Monthly-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">Four Wheeler</h6>
                                            <div class="row d-flex align-items-center">
                                                <div class="col-9">
                                                    <h3 class="f-w-300 d-flex align-items-center  m-b-0 "><i class="fa fa-car text-c-green f-30 m-r-10"></i><p class="f-w-300 m-b-0 humidity" style="font-size: 26px;"><?= $lmv_count ?></p></h3>
                                                </div>
                                                <div class="col-3 text-right">
                                                    <p class="m-b-0"></p>
                                                </div>
                                            </div>
                                            <div class="progress m-t-30" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--[ Monthly  sales section ] end-->

                                <!--[ Recent Users ] start-->
                                <div class="col-xl-8 col-md-6">
                                    <div class="card Recent-Users">
                                        <div class="card-header">
                                            <h5>Recent Users</h5>
										</div>
                                        <div class="card-block">
										<div id="chartContainer" style="height: 300px; width: 100%;">
										</div>
                                        </div>



                                    </div>
                                </div>
                                <!--[ Recent Users ] end-->

                                <!-- [ statistics year chart ] start -->
                                <div class="col-xl-4 col-md-6">
                                    <div class="card card-event">
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col">
                                                    <h5 class="m-0">Today's Capture</h5>
                                                </div>
                                                <div class="col-auto">
                                                    <label class="label theme-bg2 text-white f-14 f-w-400 float-right">7
													<!-- <?= $today_data['capture_count'] ?> -->
													</label>
                                                </div>
                                            </div>
                                            <h2 class="mt-3 f-w-300">20°C
											<!-- <?= $today_data['max_temp']!=null?$today_data['max_temp'].'°C':'' ?> -->
											<sub class="text-muted f-14">
											<!-- <?= $today_data['max_temp']!=null?'High Temperature':'Not Captured Yet' ?> -->
											</sub></h2>
											<a href="<?= base_url($controller.'/capture_list')?>"><h6 class=" label theme-bg text-white f-12 mt-4 mb-0"  style="width:60%">View more details </h6></a>

                                            <i class="feather icon-image text-c-purple f-50"></i>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-block border-bottom" style="padding:0">
                                            <div class="row d-flex align-items-center device_count">
                                                <div class="col-auto">
                                                    <i class="feather icon-camera f-30 text-c-green"></i>
                                                </div>
                                                <div class="col">
                                                    <h3 class="f-w-300">3
													<!-- <?= $total_device->camera_count ?> -->
													</h3>
                                                    <span class="d-block text-uppercase">TOTAL CAMERAS</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block" style="padding:0">
                                            <div class="row d-flex align-items-center device_count">
                                                <div class="col-auto">
                                                    <i class="feather icon-map-pin f-30 text-c-blue"></i>
                                                </div>
                                                <div class="col">
                                                    <h3 class="f-w-300">5
													<!-- <?= $total_device->location_count ?> -->
													</h3>
                                                    <span class="d-block text-uppercase">TOTAL LOCATIONS</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ statistics year chart ] end -->
                                <!--[social-media section] start-->
                                <!-- <div class="col-md-12 col-xl-4">
                                    <div class="card card-social">
                                        <div class="card-block border-bottom">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-auto">
                                                    <i class="fab fa-facebook-f text-primary f-36"></i>
                                                </div>
                                                <div class="col text-right">
                                                    <h3>12,281</h3>
                                                    <h5 class="text-c-green mb-0">+7.2% <span class="text-muted">Total Likes</span></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center card-active">
                                                <div class="col-6">
                                                    <h6 class="text-center m-b-10"><span class="text-muted m-r-5">Target:</span>35,098</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-theme" role="progressbar" style="width:60%;height:6px;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <h6 class="text-center  m-b-10"><span class="text-muted m-r-5">Duration:</span>3,539</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-theme2" role="progressbar" style="width:45%;height:6px;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card card-social">
                                        <div class="card-block border-bottom">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-auto">
                                                    <i class="fab fa-twitter text-c-blue f-36"></i>
                                                </div>
                                                <div class="col text-right">
                                                    <h3>11,200</h3>
                                                    <h5 class="text-c-purple mb-0">+6.2% <span class="text-muted">Total Likes</span></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center card-active">
                                                <div class="col-6">
                                                    <h6 class="text-center m-b-10"><span class="text-muted m-r-5">Target:</span>34,185</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-green" role="progressbar" style="width:40%;height:6px;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <h6 class="text-center  m-b-10"><span class="text-muted m-r-5">Duration:</span>4,567</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-blue" role="progressbar" style="width:70%;height:6px;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card card-social">
                                        <div class="card-block border-bottom">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-auto">
                                                    <i class="fab fa-google-plus-g text-c-red f-36"></i>
                                                </div>
                                                <div class="col text-right">
                                                    <h3>10,500</h3>
                                                    <h5 class="text-c-blue mb-0">+5.9% <span class="text-muted">Total Likes</span></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center card-active">
                                                <div class="col-6">
                                                    <h6 class="text-center m-b-10"><span class="text-muted m-r-5">Target:</span>25,998</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-theme" role="progressbar" style="width:80%;height:6px;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <h6 class="text-center  m-b-10"><span class="text-muted m-r-5">Duration:</span>7,753</h6>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-c-theme2" role="progressbar" style="width:50%;height:6px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <!--[social-media section] end-->
                                <!-- [ rating list ] starts-->
                                <div class="col-xl-4 col-md-6">
                                    <div class="card user-list">
                                        <div class="card-header">
                                            <h5>Analog Clock</h5>
                                        </div>
                                        <div class="card-block">
											<canvas id="canvas" width="280" height="280" style="background-color:#fff">
											</canvas>

                                        </div>
                                    </div>
                                </div>
                                <!-- [ rating list ] end-->
                                <div class="col-xl-8 col-md-12 m-b-30">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <!-- <li class="nav-item">
                                            <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Today</a>
                                        </li> -->
                                        <li class="nav-item">
                                            <a class="nav-link active show" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Latest 5 Readings</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Over Threshold Readings</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
									<div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Name</th>
                                                        <th>Time</th>
                                                        <th>Status</th>
                                                        <th class="text-right"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
												<?php foreach($last_five_data as $ldata) { ?>
                                                    <tr>
                                                        <td>
                                                            <h6 class="m-0"><img class="rounded-circle  m-r-10" style="width:40px;" src="assets/images/user/people.jpg" alt="activity-user"></h6>
                                                        </td>
                                                        <td>
                                                            <h6 class="m-0"> <?=$ldata->l_license_no."-".$ldata->name ?></h6>
                                                        </td>
                                                        <td>
                                                            <h6 class="m-0"><?= date('d-m-Y H:i s',strtotime($ldata->created_at)) ?></h6>
														</td>
														<?php
														$status = "Active";
														$class = 'text-c-green';
														if($ldata->status=='0'){
															$status = "Finished";
															$class = 'text-c-red';
														} ?>
                                                        <td>
                                                            <h6 class="m-0 <?= $class ?>"><?= $status ?></h6>
														</td>

                                                        <td class="text-right"><i class="fas fa-circle f-10 <?=$class ?>"></i></td>
                                                    </tr>
												<?php } ?>
                                                </tbody>
                                            </table>

                                        </div>
                                        <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>User</th>
                                                        <th>Activity</th>
                                                        <th>Time</th>
                                                        <th>Status</th>
                                                        <th class="text-right"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>

                                        </div>

                                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                            <table class="table table-hover">
                                                <thead>
												<tr>
													<th>Image</th>
													<th>Name</th>
													<th>Time</th>
													<th>Status</th>
													<th class="text-right"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
												<?php foreach($last_five_data as $ldata) { ?>
                                                    <tr>
                                                        <td>
                                                            <h6 class="m-0"><img class="rounded-circle  m-r-10" style="width:40px;" src="assets/images/user/people.jpg" alt="activity-user"></h6>
                                                        </td>
                                                        <td>
                                                            <h6 class="m-0"> <?=$ldata->l_license_no."-".$ldata->name ?></h6>
                                                        </td>
                                                        <td>
                                                            <h6 class="m-0"><?= date('d-m-Y H:i s',strtotime($ldata->created_at)) ?></h6>
														</td>
														<?php
														$status = "Active";
														$class = 'text-c-green';
														if($ldata->status=='0'){
															$status = "Finished";
															$class = 'text-c-red';
														} ?>
                                                        <td>
                                                            <h6 class="m-0 <?= $class ?>"><?= $status ?></h6>
														</td>

                                                        <td class="text-right"><i class="fas fa-circle f-10 <?=$class ?>"></i></td>
                                                    </tr>
												<?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
		<!-- [ Main Content ] end -->
<script src="https://cdn.ably.io/lib/ably.min-1.js"></script>
<script>
	const ably = new Ably.Realtime('b8vWRQ.CzEN7Q:KOIRTvIzvbbKgmX1');
	const channel = ably.channels.get('HomeLive-data');
	channel.subscribe(function(message) {
	const data = JSON.parse(message.data);
	if(data.device_id=='<?= $get_default_device->device_id ?>') {
		$('.people_count').text(data.people_count);
		$('.co2').text(data.co2);
		$('.temperature').text(data.temperature);
		$('.humidity').text(data.humidity);
	}
	console.log(data);
	});
</script>
<script type="text/javascript">
	window.onload = function () {
	CanvasJS.addColorSet("greenShades",
                [//colorSet Array

                "#9d8ed4"
                ]);
	var chart = new CanvasJS.Chart("chartContainer",
	{
		colorSet:  "greenShades",
		title:{
			text: "Driving Test Chart <?= date('d/m/Y') ?>"
		},
		data: [
		{
			type: "splineArea",
			dataPoints: [
				<?php $i = "0";
				$len = count($g_actual_data);
				foreach($g_actual_data as $actual_data) {
					$date = strtotime($actual_data->created_time)*1000;
					?>
			{ x: new Date(<?= $date ?>), y: <?= ($actual_data->people_count>$threshold_limit)? $actual_data->people_count:0?> },
			<?php	} ?>
			]
		}
		]
	});

	chart.render();
	}
</script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
		<?= include('application/views/include/footer.php'); ?>

<script>
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var radius = canvas.height / 2;
ctx.translate(radius, radius);
radius = radius * 0.90
setInterval(drawClock, 1000);

function drawClock() {
  drawFace(ctx, radius);
  drawNumbers(ctx, radius);
  drawTime(ctx, radius);
}

function drawFace(ctx, radius) {
  var grad;
  ctx.beginPath();
  ctx.arc(0, 0, radius, 0, 2*Math.PI);
  ctx.fillStyle = 'white';
  ctx.fill();
  grad = ctx.createRadialGradient(0,0,radius*0.95, 0,0,radius*1.05);
  grad.addColorStop(0, '#18eacd');
  grad.addColorStop(0.5, 'white');
  grad.addColorStop(1, '#a389d4');
  ctx.strokeStyle = grad;
  ctx.lineWidth = radius*0.1;
  ctx.stroke();
  ctx.beginPath();
  ctx.arc(0, 0, radius*0.1, 0, 2*Math.PI);
  ctx.fillStyle = '#333';
  ctx.fill();
}

function drawNumbers(ctx, radius) {
  var ang;
  var num;
  ctx.font = radius*0.15 + "px arial";
  ctx.textBaseline="middle";
  ctx.textAlign="center";
  for(num = 1; num < 13; num++){
    ang = num * Math.PI / 6;
    ctx.rotate(ang);
    ctx.translate(0, -radius*0.85);
    ctx.rotate(-ang);
    ctx.fillText(num.toString(), 0, 0);
    ctx.rotate(ang);
    ctx.translate(0, radius*0.85);
    ctx.rotate(-ang);
  }
}

function drawTime(ctx, radius){
    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    //hour
    hour=hour%12;
    hour=(hour*Math.PI/6)+
    (minute*Math.PI/(6*60))+
    (second*Math.PI/(360*60));
    drawHand(ctx, hour, radius*0.5, radius*0.07);
    //minute
    minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
    drawHand(ctx, minute, radius*0.8, radius*0.07);
    // second
    second=(second*Math.PI/30);
    drawHand(ctx, second, radius*0.9, radius*0.02);
}

function drawHand(ctx, pos, length, width) {
    ctx.beginPath();
    ctx.lineWidth = width;
    ctx.lineCap = "round";
    ctx.moveTo(0,0);
    ctx.rotate(pos);
    ctx.lineTo(0, -length);
    ctx.stroke();
    ctx.rotate(-pos);
}
</script>
