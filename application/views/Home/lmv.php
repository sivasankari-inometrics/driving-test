<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>
<style>
.td-padding {
	vertical-align: middle!important;
}
</style>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
					<!-- [ breadcrumb ] start -->
				<div class="page-header">
					<div class="page-block">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="page-header-title">
										<h5 class="m-b-10"><?= $title ?></h5>
								</div>
								<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="javascript:"><?= $home.' / '.$title ?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
					<!-- [ breadcrumb ] end -->
				<div class="main-body">
					<div class="page-wrapper">
							<!-- [ Main Content ] start -->
						<div class="row">
							<div class="col-sm-12">
								<div class="card">
									<div class="card-header">
											<h5><?= $title.' - '.$action ?></h5>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-offset-2 col-md-6">
											<div class="card-block px-0 py-3">
                        <div class="table-responsive">
												<div class="form-group">
														<label >Test Type</label>
														<select class="form-control" name="test_type" required>
														<option>-----------------------------------------select-------------------------------------------</option>
														<?php if(isset($list_data)) {
														foreach($list_data as $ldata) { ?>
															<option><?= $ldata->test_type ?></option>
														<?php }} ?>
														</select>
													</div>


															</div>
													</div>


<!-- <div class="modal fade" id="<?= $ldata->id ?>" role="dialog">
																<div class="modal-dialog">
																	<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close" data-dismiss="modal">&times;</button>
																		</div>
																		<div class="modal-body">
																			<p>Some text in the modal.</p>
																			<img class="" style="width:100%;" src="<?= $ldata->image!=''?base_url('assets/ftp_people_count/'.$ldata->image):base_url('assets/images/user/people.jpg') ?>" alt="activity-user">
																		</div>
																	</div>
																</div>
															</div> -->

													<button  class="btn btn-primary" id="button">Next</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- [ Main Content ] end -->
					</div>
				</div>
			</div>
		</div>
	</div>
  </div>
	<!-- Modal -->

	<!-- [ Main Content ] end -->
	<?= include('application/views/include/footer.php'); ?>
